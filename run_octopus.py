#!/usr/bin/env python

"""
This is wrapper script for octopus (mapping-based variant caller) by luntergroup
-https://github.com/luntergroup/octopus
"""

import os
import sys
from subprocess import call

current_directory = os.path.abspath(os.path.dirname(__file__))
reference_sequence = '/gpfs0/home/bainbridge/genomes/human_g1k_v37_decoy.fasta'
octopus = '/gpfs0/dev/software/octopus/0.4.1/bin/octopus'


def run_octopus(_args):

    if _args.command == 'trio':
        family_name = _args.input
        rows = [line.split(',') for line in _args[1].split(';')]

        dictionary = {}
        for _family in rows:
            if _family[3] != 'parents':
                dictionary['proband'] = _family[1]
            else:
                 if _family[-1] == 'male':
                     dictionary['father'] = '{},{}'.format(_family[0], _family[1])

                 if _family[-1] == 'female':
                     dictionary['mother'] = '{},{}'.format(_family[0], _family[1])

        maternal_file_name, maternal_sample = dictionary.get('mother').split(',')
        paternal_file_name, paternal_sample = dictionary.get('father').split(',')
        proband_sample = dictionary.get('proband')

        # run family trio
        run_octopus_in_trio(family_name, proband_sample, maternal_file_name, maternal_sample, paternal_file_name, paternal_sample)

    if _args.command == 'individual':
        run_octopus_individual(_args.input, _args.output)


def get_file_name(file_path):
    return os.path.splitext(os.path.basename(file_path))[0]


def run_octopus_in_trio(family_name, proband_sample, maternal_file_name, maternal_sample, paternal_file_name, paternal_sample):
    call([
        octopus,
        '-R', reference_sequence,
        '-I', proband_sample, maternal_sample, paternal_sample,
        '-M', maternal_file_name,
        '-F', paternal_file_name,
        '--output', '{}.vcf'.format(os.path.join('results', family_name)),
        '--threads', '5'
    ])


def run_octopus_individual(bam_file, name):
    if not name:
        name = get_file_name(bam_file)
    call([
        octopus,
        '-R', reference_sequence,
        '-I', bam_file,
        '--output', '{}.octopus.vcf'.format(os.path.join('results', name)),
        '--threads', '5'
    ])


def get_parser():
    """Get parser object for script coverage.py."""
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType

    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)

    subparsers = parser.add_subparsers(help='commands', dest='command')

    individual_parser = subparsers.add_parser('individual', help='germline variants in an individual')
    individual_parser.add_argument(
        '-i', '--input',
        metavar='FILE',
        dest='input',
        help='a bam file',
    )
    individual_parser.add_argument(
        '-o', '--output',
        metavar='OUTPUT-FILE',
        help='output file/directory name (default: %(default)s)',
    )

    trio_parser = subparsers.add_parser('trio', help='de novo mutations in a trio')
    trio_parser.add_argument(
        '-i', '--input',
        dest='input',
        help='WARNING! currently it take only one option (ie, Family name)',
    )

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    return parser


def main():
    args = get_parser().parse_args()
    run_octopus(args)


if __name__ == '__main__':
    main()