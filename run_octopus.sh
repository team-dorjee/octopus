#!/bin/bash

PYTHON='/usr/bin/python'
BAM_COVERAGE_FILE='/home/dtamang/projects/test-octopus/family_trio_info.txt'

LOG_DIR='/home/dtamang/projects/test-octopus/logs'
if [[ ! -e $LOG_DIR ]]; then
    mkdir $LOG_DIR
fi

declare -A array # this is the only update
while read line; do
    [[ "$line" =~ ^#.*$ ]] && continue
    if [[ ${line:0:3} == 'Fam' ]]
      then
        family=$line
    else
        IFS=', ' read -r -a row <<< "$line"
        array[$family]="${array[$family]}${array[$family]:+;}$line"
    fi
done < $BAM_COVERAGE_FILE


for key in "${!array[@]}"; do
    echo "- ${key}" 
    #python runner.py ${key} ${array[$key]}
    srun -o "$LOG_DIR/${key}.log" -J "octopus" -S 3 -p all python runner.py ${key} ${array[$key]} &
done